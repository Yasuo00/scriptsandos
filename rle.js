var fso = new ActiveXObject("Scripting.FileSystemObject");
var ts = fso.OpenTextFile(WSH.Arguments(0)); 
var str = ts.ReadAll() ;
ts.Close();

var result = "";
var i = 0;

if (typeof(str) == 'string')
{
    if(WSH.Arguments(2) == "code")
    {
        result = code(str);
    }
    else if(WSH.Arguments(2) == "decode")
    {
        result = decode(str);
    }
    else
    {
        WSH.echo("Wrong Arguments!");
        WSH.echo("Example: input.txt output.txt code/decode");
        throw new Error();
    }
}
else
{
    throw new Error("Wrong input file !");
}

var ts = fso.OpenTextFile(WSH.Arguments(1), 2, true);
ts.write(result);
ts.Close();

function code(str)
{
    var result = "";
    var n = 1;
    while(i < str.length)
    {
        while(str.charAt(i) == str.charAt(i+n))
        {
            n++;
        }
        
        var counter = n;
        
        while(n > 127)
        {
            result += ( "#"
                        + String.fromCharCode(127) 
                        + str.charAt(i));
            n -= 127;
        }
        if(n > 3 || str.charAt(i) == '#')
        {
            result += ( "#"
                        + String.fromCharCode(n) 
                        + str.charAt(i));
        }
        else
        {
            result += str.substr(i,n);
        }
        i += counter;
        n = 1;
    }
    return result;
}

function decode(str)
{
    var result = "";
    while(i < str.length)
    {
        if(str.charAt(i) == '#')
        {
            for(iCount = 0 ;iCount < str.charCodeAt(i+1); iCount++)
            {
                result += str.charAt(i+2);
            }
            i += 2;
        }
        else
            result += str.charAt(i);
        
        i++;
    }
    return result;
}

