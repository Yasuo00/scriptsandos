fso = new ActiveXObject("Scripting.FileSystemObject"); 
alphabetSize = 26;
startLetter = "a".charCodeAt(0);

if(WScript.Arguments(0) == "SFT")
{
	fh = fso.OpenTextFile(WScript.Arguments(1)); 
	str = fh.ReadAll();
	str = str.toLowerCase();
	fh.Close();
	SetFrequencyTable(str, WScript.Arguments(2));
}
else if(WScript.Arguments(0) == "CODE")
{
	fh = fso.OpenTextFile(WScript.Arguments(1))
	str = fh.ReadAll();
	fh.Close();
	str = str.toLowerCase();
	str = Code(str,parseFloat(WScript.Arguments(2)));
	fh = fso.OpenTextFile(WScript.Arguments(3),2,true);
	fh.Write(str);
	fh.Close();
}
else if(WScript.Arguments(0) == "DECODE")
{
	fh = fso.OpenTextFile(WScript.Arguments(1))
	str = fh.ReadAll();
	fh.Close();
	str = str.toLowerCase();
	str = Code(str,26 - (parseFloat(WScript.Arguments(2))) % 26);
	fh = fso.OpenTextFile(WScript.Arguments(3),2,true);
	fh.Write(str);
	fh.Close();
}
else if(WScript.Arguments(0) == "DECODEFr")
{
	fh = fso.OpenTextFile(WScript.Arguments(1))
	str = fh.ReadAll();
	fh.Close();
	str = str.toLowerCase();

	
	fh = fso.OpenTextFile(WScript.Arguments(2))
	freqStr = fh.ReadAll();
	freqTableStr = freqStr.split("\r\n");
	freqTable = new Array();
	for(i = 0 ; i < freqTableStr.length; i++)
	{
		temp = freqTableStr[i].split("-");
		freqTable[temp[0]] = parseFloat(temp[1]);
	}
	freqTable.pop();

	str = decodeWithFrTable(str,freqTable);
	fh = fso.OpenTextFile(WScript.Arguments(3),2,true);
	fh.Write(str);
	fh.Close();
}

function Code(str, shift)
{
	shift %= alphabetSize;
	codedStr= new Array();
	for(i = 0; i < str.length; i++)
	{
		if(str.charAt(i) >= "a" && str.charAt(i) <= "z")
			codedStr.push(String.fromCharCode(startLetter + (str.charCodeAt(i) + shift - startLetter) % alphabetSize));
		else
			codedStr.push(str.charAt(i));
	}
	return codedStr.join("");
}

function SetFrequencyTable(str, nameOfFile)
{
	noSpaceCount = 0;
	frequencyTable = new Array();
	for(i = 0; i < str.length; i++)
	{
		if(str.charAt(i) >= "a" && str.charAt(i) <= "z")
		{
			if(frequencyTable[str.charAt(i)])
				frequencyTable[str.charAt(i)]++;
			else
				frequencyTable[str.charAt(i)] = 1;
			noSpaceCount++;
		}
	}
	for(e in frequencyTable)
		frequencyTable[e] /= noSpaceCount;
	for(i = 0; i < alphabetSize - 1; i++)
	{
		if(!frequencyTable[String.fromCharCode(startLetter + i)])
			frequencyTable[String.fromCharCode(startLetter + i)] = 0;
	}

	fso2 = new ActiveXObject("Scripting.FileSystemObject");
	fh2= fso2.OpenTextFile(nameOfFile,2,true);
	for(e in frequencyTable)
		fh2.WriteLine(e+"-"+frequencyTable[e])

	WSH.echo("Frequency table seted!");
}

function decodeWithFrTable(str, freqTable)
{
	currentFreqTable = new Array();
	noSpaceCount = 0;
	for(i = 0; i < str.length; i++)
	{
		if(str.charAt(i) >= "a" && str.charAt(i) <= "z")
		{
			noSpaceCount++;
			if(currentFreqTable[str.charAt(i)])
				currentFreqTable[str.charAt(i)]++;
			else
				currentFreqTable[str.charAt(i)] = 1;
		}
	}
	for(e in currentFreqTable)
		currentFreqTable[e] /= noSpaceCount;

	for(i = 0; i < alphabetSize; i++)
	{
		if(!currentFreqTable[String.fromCharCode(i + startLetter)])
			currentFreqTable[String.fromCharCode(i + startLetter)] = 0;
	}

	minColision = 100;
	minShift = 0;
	for(i = 0; i < 26; i++)
	{
		ColisionTemp = 0;
		for(e in currentFreqTable)
		{
			ColisionTemp += Math.abs(currentFreqTable[e] - freqTable[String.fromCharCode(startLetter + (e.charCodeAt(0) + i - startLetter) % alphabetSize)]);
		}
		//WSH.echo(i + " " + ColisionTemp);
		if(ColisionTemp < minColision)
		{
			minShift = i;
			minColision = ColisionTemp;
		}
	}
	return(Code(str,minShift));
}
