CURRENT_PATH=$PATH
IFS=:
TEMPORARY_PATH=""
for dir in $CURRENT_PATH; do 
	if [ -d "$dir" ]; then
		for file in "$dir"/*; do
			echo $file
			if [ -f "$file" ] && [ -x "$file" ]; then 
				TEMPORARY_PATH=$TEMPORARY_PATH$dir:
				break
			fi
		done
	fi
done
IFS=
echo "$TEMPORARY_PATH"