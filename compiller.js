var fso = new ActiveXObject("Scripting.FileSystemObject");
var ts = fso.OpenTextFile(WSH.Arguments(0)); 
var programm = ts.ReadAll();

var stackOfCommands = new Array();
var commandList = programm.split('\n');
var variableList = new Array();
variableList["zero"] = 0;
variableList["one"] = 1;
var labelList = new Array();

for(var i in commandList)
{
    var tokens = getTokens(commandList[i]);
    switch(tokens[0])
    {
        case 'label':
            labelList[tokens[1]] = stackOfCommands.length;
            break;
        case 'var':
            variableList[tokens[1]] = tokens[2];
            break;
        default:
            if(tokens.length != 0)
                stackOfCommands.push(tokens);
    }
}


var stackPointer = 0;
while(stackPointer < stackOfCommands.length)
{
    var currentCommand = stackOfCommands[stackPointer];
    switch(stackOfCommands[stackPointer][0])
    {
        case 'add':
            var c = parseInt(variableList[currentCommand[1]]) + parseInt(variableList[currentCommand[2]]);
            variableList[currentCommand[1]] = c;
            stackPointer++;
            break;
        case 'dec':
            var c = parseInt(variableList[currentCommand[1]]) - parseInt(variableList[currentCommand[2]]);
            variableList[currentCommand[1]] = c;
            stackPointer++;
            break;
        case 'jmpz':
            if(variableList[currentCommand[1]] == 0)
                stackPointer = labelList[currentCommand[2]];
            else
                stackPointer++;
            break;
        case 'drw':
            WSH.echo(variableList[currentCommand[1]]);
            stackPointer++;
            break;  
        case 'max':
            var c = Math.max(variableList[currentCommand[1]],variableList[currentCommand[2]]);
            variableList[currentCommand[1]] = c;
            stackPointer++;
            break;
        case 'pow':
            var c = parseInt(variableList[currentCommand[1]]) * parseInt(variableList[currentCommand[2]]);
            variableList[currentCommand[1]] = c;
            stackPointer++;
            
    }
}

function getTokens(command)
{
    var i = 0;
    var tokens = [];

    while(i < command.length)
    {
        if(command.charAt(i) != 0)
        {
            var count = 0;
            while(i + count < command.length && command.charAt(i+count) != " ")
                count++;
            tokens.push(command.substr(i,count));
            i += count;
        }
        else
        {
            i++;
        }
    }
    return tokens;
}