var str = WScript.StdIn.ReadLine();

evalStack = ToTokens(str);
var result = new Array(); 
var stack = new Array(); 

resultStr = evalStack.join(",");
	WSH.echo("EvalStack: ",resultStr);

for (i = 0; i < evalStack.length; i++) 
{ 
	if (evalStack[i] == "(") 
	{
		stack.push("(");
		continue;
	}
	if (evalStack[i] == ")")
	{
		while (stack[stack.length-1] != "(") 
			result.push(stack.pop()); 
		stack.pop();
		continue;
	}
	if (IsOperator(evalStack[i])) 
	{ 
		if (stack.length == 0) 
		{ 
			stack.push(evalStack[i]); 
			continue; 
		} 
		while (Priority(stack[stack.length - 1]) >= Priority(evalStack[i]) && stack[stack.length-1] != "(")
			result.push(stack.pop()); 
		stack.push(evalStack[i]); 
		continue;
	}
	result.push(evalStack[i]);
} 
while (stack.length > 0) 
	result.push(stack.pop()); 

resultStr = result.join(",");
	WSH.echo("Polish numbers: ",resultStr);


WSH.echo("Result:" + " " + MyEvaluator(result));


function MyEvaluator(stack)
{
	i = 1;
	while(stack.length > 1)
	{
		if(Operation(stack[i],stack[i-2],stack[i-1]))
		{
			if(stack[i] == "u")
			{
				stack.splice(i-1,2,Operation(stack[i],stack[i-2],stack[i-1]));
				i -= 1;
			}
			else
			{
				stack.splice(i-2,3,Operation(stack[i],stack[i-2],stack[i-1]));
				i -= 2;			
			}
		}
		i++;
	}
	return stack[0];
}

function Operation(operator,a,b) 
{ 
	switch(operator){
		case "+": return parseFloat(a) + parseFloat(b);
		case "-": return parseFloat(a) - parseFloat(b);
		case "*": return parseFloat(a) * parseFloat(b);
		case "/": return parseFloat(a) / parseFloat(b);
		case "^": return Math.pow(parseFloat(a),parseFloat(b));
		case "u": return parseFloat(b) * "-1";
	}
	return null;
}


function Priority(str)
{
	if(str == "+" || str == "-")
		return 1;
	if(str == "*" || str == "/")
		return 2;
	if(str == "^")
		return 3;
	if(str == "u")
		return 4;
}

function ToTokens(str)
{
	res = new Array();
	for(i = 0; i < str.length; i++)
	{
		if(str.charAt(i) != " ")
		{
			if(str.charAt(i) >= "0".charAt(0) && str.charAt(1) <= "9".charAt(0))
			{
				shift = 0;
				while(i + shift < str.length && ((str.charAt(i + shift) >= "0".charAt(0) && str.charAt(shift) <= "9".charAt(0)) || str.charAt(i+shift) == ".".charAt(0)))
					shift++;
				res.push(str.substr(i,shift));
				i += shift - 1;
			}
			else if (IsValidToken(str.charAt(i)))
			{
				if(str.charAt(i) == "-".charAt(0))
				{
					if(IsOperator(res[res.length-1]) || res[res.length-1] == "underfined")
						res.push("u")
					else
						res.push("-");
				}
				else
				{
					res.push(str.charAt(i));
				}
			}
			else
				throw new Error("Bad Input");
		}
	}
	return res;
}

function IsValidToken(str)
{
	return (str == "+" || str == "-" || str == "^" || str == "*" || str == "/" || str == ")" || str == "(");
}
function IsOperator(str)
{
	return (str == "+" || str == "-" || str == "^" || str == "*" || str == "/" || str == "(" || str == "u");
}