#!/bin/bash 

if [ "$1" = "-h" -o "$1" = "--help" ] 
then
	echo "This programm takes immage with and make gif animation" 
	echo "First argument is angle of rotation in DEGREES"
	echo "Second argument is duration of animation in MILISECONDS"
	echo "Third argument is image name without jpeg"
fi
if ! [[ $1 =~ ^[0-9]+$ ]]  
then
	echo "Wrong first argument!"
	exit 0;
fi
if ! [[ $2 =~ ^[0-9]+$ ]]  
then
	echo "Wrong second argument!"
	exit 0;
fi
if [ "$3" = "" ] 
then
	echo "Enter image name without jpeg for last argument!"
	exit 0;
fi
if ! [ -f "$3.jpeg" ] 
then 
	echo "Image can not be found"
	exit 0;
fi
directoryName=$RANDOM
while [ -d $directoryName ] 
do
	directoryName=$RANDOM
done
mkdir $directoryName
convert "$3.jpeg" -fill xc:white $directoryName/white.jpeg
anglePerMilisecond=$(($1/$2))
for (( i = 0; $i < $2 ; i++ )) 
do 
	nameOfTempPicture="$3_${i}.jpeg"
	convert "$3.jpeg" -rotate $(($anglePerMilisecond*$i)) "$directoryName/$nameOfTempPicture.jpeg" 
	composite -gravity center "$directoryName/$nameOfTempPicture.jpeg" "$directoryName/white.jpeg" "$directoryName/$nameOfTempPicture.jpeg"  
done

convert  "$directoryName/$3_*.jpeg" -loop 0 "$3.gif"
sudo rm -rf $directoryName

 


