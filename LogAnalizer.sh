#!/bin/bash
NOW=$(date +"%y-%m-%d")
t=1;
for i in $( cut -d '|' -f 2 $1 | cut -d ' ' -f 2); do
	let yearl=10#$(echo "$i" | cut -d '-' -f 1 | rev | cut -c -2 | rev)
	let yearn=10#$(echo "$NOW" | cut -d '-' -f 1)
	let monthl=10#$(echo "$i" | cut -d '-' -f 2)
	let monthn=10#$(echo "$NOW" | cut -d '-' -f 2)
	let dayl=10#$(echo "$i" | cut -d '-' -f 1 | rev | cut -c -3 | rev)
	let dayn=10#$(echo "$NOW" | cut -d '-' -f 3)
	dayn=$(($dayn + $yearn * 365 + $monthn * 30))
	dayl=$(($dayl + $yearl * 365 + $monthl * 30))
	if [ $((dayn - dayl)) -lt 30 ]; then
		echo $(cut -d $'\n' -f $t $1)
	fi
	t=$((t + 1))
done
