
WSH.echo("Enter the string:");
str=WScript.StdIn.ReadLine();



alph=new Array();

for(i=0;i<str.length;i++)
    alph[str.charAt(i)]=0;
for(i=0;i<str.length;i++)
    alph[str.charAt(i)]++;
var len = 0;
for (var item in alph)
{
    len+=1;
}
if(len < 2)
{
    WSH.echo(0);
}
else
{
    var entropy = 0;
    for(var item in alph)
    {
        alph[item] = alph[item] / str.length;
        entropy += (-alph[item]) *  BaseLog(len,alph[item]);
    }
    WSH.echo(entropy);  
}
function BaseLog(x,y)
{
    return Math.log(y)/Math.log(x);
}