fso = new ActiveXObject("Scripting.FileSystemObject");
fh = fso.OpenTextFile("text.txt");
str = fh.ReadAll();
substr = WScript.StdIn.ReadLine();
fh.Close();

Bench(new TestCase(str,substr,RabinKarpHash,ReHashRabinKarpHash), HashForce , "RabinKarp");
Bench(new TestCase(str,substr,SimpleHash,ReHashSimpleHash), HashForce , "SimpleHash");
Bench(new TestCase(str,substr,QuadHash,ReHashQuadHash), HashForce , "QuadHash");
Bench(new TestCase(str,substr,RabinKarpHash,ReHashRabinKarpHash), BruteForce , "BruteForce");

list = HashForce(str,substr,RabinKarpHash,ReHashRabinKarpHash);
for(a in list)
	WSH.echo(list[a]);

function TimeChek(testCase, func)
{
	time = new Date();
	func(testCase.str,testCase.substr,testCase.hash,testCase.rehash);
	return -(time - new Date());
}
function Bench(testCase, func, name)
{
	middle = 0;
	for(i = 0; i < 10; i++)
		middle += TimeChek(testCase,func);
	middle /= 10;
	WSH.echo(name +":" + middle + " miliseconds in avarage.");
}



function TestCase(str, subst, hash, rehash)
{
	this.str = str;
	this.substr = subst;
	this.hash = hash;
	this.rehash = rehash;
}

function BruteForce(str,substr)
{
	var list = new Array();
	for(i = 0; i < str.length - substr.length + 1; i++)
	{
		count = 0
		for(; count < substr.length; count++)
		{
			if(substr.charAt(count) != str.charAt(i + count))
				break;
		}
		if(count == substr.length)
			list.push(i);
	}
	return list;
}

function HashForce(str, substr, Hash, ReHash)
{
	var list = new Array();
	var CompHash = Hash(substr);
	var CurrentHash = Hash(str.substr(0,substr.length));
	for(i = 0; i < str.length - substr.length + 1; i++)
	{
		if(CurrentHash == CompHash)
		{
			count = 0;
			for(; count < substr.length; count++)
			{
				if(substr.charAt(count) != str.charAt(i + count))
					break;
			}
			if(count == substr.length)
				list.push(i);
		}
		CurrentHash = ReHash(CurrentHash,str.substr(i,1),str.substr(i + substr.length,1), substr.length);
	}
	return list;
}


///////////////////////////////////// SimpleHash
function SimpleHash(str)
{
	sum = 0;
	for(i = 0; i < str.length; i++)
		sum += str.charCodeAt(i);
	return sum;
}

function ReHashSimpleHash(hs,prev,next,strLen)
{
	hs -= prev.charCodeAt(0) - next.charCodeAt(0);
	return hs;
}

///////////////////////////////////// QuadraticHash
function QuadHash(str)
{
	sum = 0;
	for(i = 0; i < str.length; i++)
		sum += Math.pow(str.charCodeAt(i),2);
	return sum;
}

function ReHashQuadHash(hs,prev,next,strLen)
{
	hs -= Math.pow(prev.charCodeAt(0),2) - Math.pow(next.charCodeAt(0),2);
	return hs;
}
/////////////////////////////////////////// RabinKarpHash
function RabinKarpHash(str)
{
	sum = 0;
	for(i = 0; i < str.length; i++)
		sum += str.charCodeAt(i) * Math.pow(2,str.length - 1 - i);
	return sum;
}
function ReHashRabinKarpHash(hs,prev,next,strLen)
{
	hs -= prev.charCodeAt(0) * Math.pow(2,strLen - 1);
	hs *= 2;
	hs += next.charCodeAt(0);
	return hs;
}






