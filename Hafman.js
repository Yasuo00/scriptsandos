function Node(name, count , code , nextId)
{
    this.code = code; 
    this.name = name;
    this.used = 0;
    this.count = count;
    this.nextId;
}

str=WScript.StdIn.ReadLine();
fTable = new Array();

for(i = 0; i < str.length; i++)
    fTable[str.charAt(i)] = 0;
for(i = 0; i < str.length; i++)
    fTable[str.charAt(i)] += 1;

var NodeList = new Array();
var CodeTable = new Array();
for(var i in fTable)
{
    var newNode = new Node(i, fTable[i], null, null);
    NodeList.push(newNode);
}
if(NodeList.length == 1)
{
    WSH.echo(NodeList[0].name + '\t' + '1');
    CodeTable[NodeList[0].name] = '1';
}
else
{
    var itertions = NodeList.length - 1;
    for(var i = 0;i < itertions;i++)
    {
        var minIndex1 = getMinIndex(NodeList);
        NodeList[minIndex1].used = 1;
    
        var minIndex2 = getMinIndex(NodeList);
        NodeList[minIndex2].used = 1;
    
        NodeList.push(new Node(NodeList[minIndex1].name + NodeList[minIndex2].name,
                            NodeList[minIndex1].count + NodeList[minIndex2].count,null,null));
                            
        NodeList[minIndex1].code = '1';
        NodeList[minIndex2].code = '0';
        NodeList[minIndex1].nextId = NodeList.length - 1;
        NodeList[minIndex2].nextId = NodeList.length - 1;
    }
    var j = 0;
    for(var i in fTable)
    {
    
        var startID = j;
        var ICode = NodeList[startID].code;
        while(NodeList[startID].nextId != null)
        {
            if(NodeList[startID].code != 'null')
            {
                startID = NodeList[startID].nextId;
                ICode = NodeList[startID].code + ICode;  
                }            
        }
        WSH.echo(i + '\t' +  ICode.substr(4));
        CodeTable[i] = ICode.substr(4);
        j++;
    }
}
var retStr = "";
for (i = 0; i < str.length; i++)
{
    retStr += CodeTable[str.charAt(i)];
}
WSH.echo("Coded message:  " + retStr); 

var DecodeTable = swap(CodeTable);
code = "";
decode = "";
for(i = 0;i < retStr.length;i++)
{
  code += retStr.charAt(i);
  if (code in DecodeTable)
  {
      decode+=DecodeTable[code];
      code="";
  }
}
WSH.echo("Decoded message:" + decode);


function swap(json){
  var ret = {};
  for(var key in json){
    ret[json[key]] = key;
  }
  return ret;
}

function getMinIndex(NodeList)
{
    var minCount1 = str.length + 1;
    var minIndex1 = 0;
    for(var j = 0; j < NodeList.length; j++)
    {
        if(NodeList[j].used != 1 && NodeList[j].count < minCount1)
        {
            minCount1 = NodeList[j].count;
            minIndex1 = j;
        }  
    }
    return minIndex1;
}