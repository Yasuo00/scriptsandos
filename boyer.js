fso = new ActiveXObject("Scripting.FileSystemObject"); 
fh = fso.OpenTextFile(WScript.Arguments(0));
var text = fh.ReadAll();
fh.Close();
fh = fso.OpenTextFile(WScript.Arguments(1));
var pattern = fh.ReadAll();
fh.Close();

var BadTable = GetBadCharacterRuleTable(pattern);
var GoodTable = GetGoodSuffixRuleTable(pattern);

var Result = new Array();

var patternLength = pattern.length;
var textLength = text.length;

for(i = pattern.length - 1; i < textLength;)
{
	j = 0
	for(; j < patternLength; j++)
	{
		if(pattern.charAt(patternLength - j -1) != text.charAt(i - j))
		{
			maxShifBad = BadTable[patternLength - j -1][text.charAt(i - j)];
			if(!maxShifBad)
				maxShifBad = patternLength - j + 1;
			maxShifGood = GoodTable[pattern.substr(j,patternLength - j - 1)];
			if(!maxShifGood)
				maxShifGood = patternLength - j;
			WSH.echo(Math.max(maxShifBad,maxShifGood) - 1);
			i += Math.max(maxShifBad,maxShifGood) - 1;
			break; 
		}
	}
	if(j == patternLength)
	{
		WSH.echo("YES");
		Result.push(i - patternLength + 1);
		i++;
	}
}

WSH.echo("Result:" + Result.join(" "));
WSH.echo(pattern);
for(e in GoodTable)
{
	WSH.echo(e + " " + GoodTable[e]);
}


function GetBadCharacterRuleTable(str)
{
	var BadTable = new Array();
	for(i = str.length - 1; i >= 0; i--)
	{
		BadTable[i] = new Array();
		for(j = i; j >= 0; j--)
		{
			var token = str.charAt(j)
			if(!BadTable[i][token] && str.charAt(i) != str.charAt(j))
				BadTable[i][token] = i - j - 1;
		}
	}
	return BadTable;
}

function GetGoodSuffixRuleTable(str)
{
	var GoodSuffixRuleTable = new Array();
	for(i = 1; i < str.length; i++)
	{
		var suffix = str.substr(str.length  - i,i);

		for(j = str.length - i - 1; j >= 0; j--)
		{
			if(str.substr(j,i) == suffix)
			{
				GoodSuffixRuleTable[suffix] = Math.abs(str.length - i - j - 1);
				break;
			}
		}
		if(!GoodSuffixRuleTable[suffix] && suffix.length <= str.length / 2 + 1)
			GoodSuffixRuleTable[suffix] = 0;
	}
	for(i = 2; i < str.length; i++)
	{
		var suffix = str.substr(str.length  - i,i);
		if(!GoodSuffixRuleTable[suffix])
			GoodSuffixRuleTable[suffix] = GoodSuffixRuleTable[suffix.substr(1,suffix.length - 1)];

	}
	return GoodSuffixRuleTable;
}
