﻿using System;
using System.Collections.Generic;

 
namespace Monobilliards
{
	public class Node<Type>
	{
		public Type Value { get; set; }
		public Node<Type> Next;
		
		public Node(Type value)
		{
			Value = value;
			Next = null;
		}
	}
	
	public class MyQueue<Type> 
	{
		private Node<Type> head = null;
		private Node<Type> tail = null;

		private int count = 0;
		public int Count{get { return count; }}

		public void Enqueue(Type value)
		{
			if (head == null)
			{
				head = new Node<Type>(value);
				tail = head;
				count = 1;
			}
			else
			{
				tail.Next = new Node<Type>(value);
				tail = tail.Next;
				count++;
			}
		}

		public Type Dequeue()
		{
			try
			{
				if(head == null)
					throw new Exception("Queue is empty but you try to Dequeue.")
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				throw;
			}

			Type ret = head.Value;
			head = head.Next;
			count--;
			if (head == null)
				tail = null;
			
			return ret;
		}

		public Type First()
		{
			try
			{
				if (head == null)
					throw  new Exception("Queue is empty but you try to get first item.");
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				throw;
			}				
			return head.Value;
		}
		public Type Last()
		{
			try
			{
				if (tail == null)
					throw  new Exception("Queue is empty but you try to get last item.");
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				throw;
			}				
			return tail.Value;
		}
	}
	
	public class Monobilliards : IMonobilliards
	{
 		public bool IsCheater(IList<int> inspectedBalls)
 		{
			var resultStack = new Stack<int>();
			var queue = new MyQueue<int>();
			var searchNumber = 1;

			for (int i = 0; i < inspectedBalls.Count; i++)
			{
				if (inspectedBalls[i] == searchNumber)
				{
					
				}
			}
 		}
	}
}